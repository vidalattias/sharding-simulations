use crate::shard::Universe;

pub fn build_shards<'a>(depth:u32) -> Universe<'a>
{
    Universe::new(depth)
}
