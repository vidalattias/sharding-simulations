use crate::node::Node;
use crate::tx_counter;

use crate::TIME;

pub struct Transaction<'a>
{
    issuer : &'a Node<'a>,
    id : usize,
    timestamp : u32,
    validation_date : Vec<u32>,
}

impl<'a> Transaction<'a>
{
    pub fn new<'b>(issuer : &'b Node<'b>) -> Transaction<'b>
    {
        Transaction{issuer,
            id: tx_counter::next(),
            timestamp: unsafe{TIME as u32},
            validation_date: Vec::new(),
        }
    }
}