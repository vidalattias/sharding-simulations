#[macro_use]
extern crate simple_counter;

pub mod node;
pub mod shard;
pub mod transaction;
pub mod utils;

static NB_CHILDS:u32 = 2;
static DISSEMINATION_RATE:f64 = 2.;
static mut TIME:f64 = 0.;

generate_counter!(shard_counter, usize);
generate_counter!(node_counter, usize);
generate_counter!(tx_counter, usize);

fn main() {
    let universe = utils::build_shards(2);

    universe.get_root().print_structure(0);

    unsafe{TIME=0.};


    let shards_list : Vec<&shard::Shard> = universe.build_shards_list();


    let node = node::Node::new(node_counter::next());

    while unsafe{TIME < 100.}
    {
        node.issue_transaction();
        unsafe{TIME += 1.};
    }
    
    println!("On regarde les shards :");

    for i in &shards_list
    {
        println!("{}", i.get_id());
}
}