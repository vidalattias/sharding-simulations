use crate::transaction::Transaction;
use crate::shard::Shard;

pub struct Node<'a>
{
    id: usize,
    participating_tangles : Vec<&'a Shard<'a>>,
}

impl<'a> Node<'a>
{
    pub fn new<'b>(id: usize) -> Node<'b>
    {
        Node{id, participating_tangles:Vec::new()}
    }

    pub fn issue_transaction(&self)
    {
        let new_tx = Transaction::new(&self);
    }
}