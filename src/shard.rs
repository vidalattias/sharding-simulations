use crate::transaction::Transaction;
use crate::node::Node;

use crate::NB_CHILDS;
use crate::DISSEMINATION_RATE;

use crate::shard_counter;

pub struct Universe<'a>
{
    root : Shard<'a>,
}

pub struct Shard<'a>
{
    id: usize,
    childs: Vec<Shard<'a>>,
    parent: Option<&'a Shard<'a>>,
    participating_nodes : Vec<&'a Node<'a>>,
    dissemination_rate : f64,
    transactions : Vec<&'a Transaction<'a>>,
    referencer: Option<&'a Node<'a>>,
}

impl<'a> Shard<'a>
{   
    pub fn new_empty<'b>(parent:Option<&'a Shard<'a>>) -> Shard<'b>
    {
        Shard{
            id:shard_counter::next(),
            childs:Vec::new(),
            parent: None,
            participating_nodes : Vec::new(),
            dissemination_rate : DISSEMINATION_RATE,
            transactions: Vec::new(),
            referencer: None,
        }
    }

    pub fn create_empty_shards(shard: &mut Shard, depth:u32)
    {
        for _ in 0..NB_CHILDS
        {
            shard.childs.push(Shard::new_empty(Some(shard)));
        }

        if depth > 0
        {
            for s in &mut shard.childs
            {
                Shard::create_empty_shards(s, depth-1);
            }
        }
    }

    pub fn print_structure(&self, depth: usize)
    {
        let repeated = "\t".repeat(depth);
        println!("{}{}", repeated, self.id);

        for shard in &self.childs
        {
            shard.print_structure(depth+1);
        }

    }

    pub fn build_shards_list(&'a self) -> Vec<&'a Shard<'a>>{
        let mut ret_vec = vec![self];

        for shard in &self.childs
        {
            ret_vec.append(&mut shard.build_shards_list());
        }

        ret_vec
    }

    pub fn set_referencer(&mut self, referencer: &'a Node<'a>)
    {
        self.referencer = Some(referencer);
    }

    pub fn get_id(&self) -> usize
    {
        self.id
    }
}

impl<'a> Universe<'a>
{
    pub fn new<'b>(depth : u32) -> Universe<'b>
    {
        let mut shard = Shard::new_empty(None);

        if depth > 0{
            Shard::create_empty_shards(&mut shard, depth-1);
        }

        Universe{
            root: shard,
            }
    }

    pub fn build_shards_list(&'a self) -> Vec<&'a Shard<'a>>{
        let ret_vec: Vec<&Shard> = self.root.build_shards_list();
    

        ret_vec
    }

    pub fn get_root(&self) -> &Shard
    {
        &self.root
    }
}